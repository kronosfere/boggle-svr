# boggle-svr

# How to run

- Create a python3 virtual environment
- `pip install -r requirements.txt`
- `export FLASK_APP=flask_app`
- `python -m flask run`
