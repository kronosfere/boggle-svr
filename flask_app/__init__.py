# import os
from flask import Flask, request
from flask_cors import CORS

import json

from . import utils

solver = utils.Solver()


def create_app():
    app = Flask(__name__, instance_relative_config=True)
    CORS(app)

    @app.route('/solve', methods=['GET'])
    def solve_url():
        input = json.loads(request.args.get('board').lower())
        solver.load_board(input)
        solver.solve()

        return json.dumps(list(solver.found))

    return app
