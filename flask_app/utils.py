import os
import sys
import io

import pygtrie as trie

from copy import deepcopy
from functools import reduce

BOARD_SIZE = 4
# bit masks
HAS_VALUE = trie.Trie.HAS_VALUE
HAS_SUBTRIE = trie.Trie.HAS_SUBTRIE


def trie_reducer(words_dict, val):
    words_dict[val[:-1]] = True
    return words_dict


def check_bounds(val):
    return val < 0 or val >= BOARD_SIZE


def print_board(board):
    for y in range(0, 4):
        print(board[y])


class Solver(object):

    def __init__(self):
        self.words_dict = trie.CharTrie()
        self.board = []
        self.found = set()
        self.cache = {}
        self.cur_cache = {}
        self.load_dict('dictionary.txt')

    # Assume dictionary is sanitized as it is loaded server-side
    def load_dict(self, file):
        fd = io.open(f'{sys.path[0]}/{file}', "r", encoding="utf-8")
        ret = fd.readlines()
        self.words_dict = reduce(trie_reducer, ret, self.words_dict)

    def load_board(self, input):
        # clear old board
        self.board = []
        self.cur_cache = {}
        for i in range(BOARD_SIZE):
            self.board.append(input[i * BOARD_SIZE:(i + 1) * BOARD_SIZE])

    def solve(self):
        self.found = set()
        for y in range(0, BOARD_SIZE):
            for x in range(0, BOARD_SIZE):
                self.check_val(x, y, self.board[y][x])

        return self.found

    def reduce_paths(self, new_paths, key):
        val = self.words_dict.has_node(key)
        if val & HAS_VALUE:
            self.found.add(key)
        if val & HAS_SUBTRIE:
            new_paths.add(key)
        return new_paths

    def resolve_wildcard(self, new_paths, key):
        # _get_node returns tuple of node and traversed path
        children = self.words_dict._get_node(key)[0].children.keys()
        new_paths.update(map(lambda val: key + val, children))
        return new_paths

    def check_val(self, x, y, cur_str='', paths={''}):
        # check bounds and check if traversed
        if check_bounds(x) or check_bounds(y) or self.board[y][x][0] == '~':
            return

        new_str = cur_str + self.board[y][x]
        # cache paths before adding result
        try:
            new_paths = self.cache[new_str]
        except KeyError:
            # Initialize new set
            if self.board[y][x] == '*':
                new_paths = reduce(self.resolve_wildcard, paths, set())
            else:
                #  map to generate possible paths at this point
                new_paths = set(map(lambda val: val + self.board[y][x], paths))
            self.cache[new_str] = new_paths

        # check and cache paths after adding result (cull double add)
        try:
            new_paths = self.cur_cache[new_str]
        except KeyError:
            new_paths = reduce(self.reduce_paths, new_paths, set())
            self.cur_cache[new_str] = new_paths

        # add ~ to signify checked
        self.board[y][x] = '~' + self.board[y][x]
        if len(new_paths):
            for range_y in range(-1, 2):
                for range_x in range(-1, 2):
                    self.check_val(x + range_x, y + range_y, new_str,
                                   new_paths)

        # reset on return
        self.board[y][x] = self.board[y][x][-1]
        return
